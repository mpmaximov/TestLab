CREATE TABLE Phones (
  Id int CONSTRAINT firstkey PRIMARY KEY,
  Name        VARCHAR(100),
  Producer    Varchar(100),
  Price       MONEY,
  Count       MONEY,
  Margin      MONEY
);

CREATE TABLE Sales (
  Id int CONSTRAINT firstkey PRIMARY KEY,
  PhoneId        int,
  Sum    MONEY,
  Dt       TIMESTAMP
);