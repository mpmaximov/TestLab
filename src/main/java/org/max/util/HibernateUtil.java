package org.max.util;

import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;

import java.io.File;

/**
 * Created by mmaximov on 10/29/15.
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure(new File("hibernate.cfg.xml")).buildSessionFactory();
        } catch (Exception ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory connect() {
        return sessionFactory;
    }

    public static void disconnect() {
        sessionFactory.close();
    }

}