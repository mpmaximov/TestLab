package org.max.domain;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by mmaximov on 10/31/15.
 */


@NamedQueries({
        @NamedQuery(name = "getAllPhones", query = "from Phones")
})

@Entity(name = "Phones")
public class Phone {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @Column(length = 100)
    private String name;

    @ManyToOne(targetEntity = Producer.class)
    private Producer producer;

    @Column
    private BigDecimal price;

    @Column
    private int count;

    @Column
    private BigDecimal margin;

    public Phone() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getMargin() {
        return margin;
    }

    public void setMargin(BigDecimal margin) {
        this.margin = margin;
    }

    /*public static class Builder {
        private long id;
        private String name;
        private String producer;
        private BigDecimal price;
        private int count;
        private BigDecimal margin;

        Builder() {}
        public Builder id (long id) { this.id = id;  return this; }
        public Builder name (String name) { this.name = name;  return this; }
        public Builder producer (String producer) { this.producer = producer;  return this; }
        public Builder price (BigDecimal price) { this.price = price;  return this; }
        public Builder count (int count) { this.count = count;  return this; }
        public Builder margin (BigDecimal margin) { this.margin = margin;  return this;
        public Phone build() { return new Phone(this); }


    }*/

    /*private Phone(Builder b) {
        this.id = b.id;
        this.name = b.name;
        this.producer = b.producer;
        this.price = b.price;
        this.count = b.count;
        this.margin = b.margin;
    }*/


}
