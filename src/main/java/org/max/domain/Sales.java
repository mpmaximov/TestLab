package org.max.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;

/**
 * Created by mmaximov on 11/1/15.
 */
@Entity(name = "Sales")
public class Sales {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Id;

    @ManyToOne(targetEntity = Phone.class)
    private Phone PhoneId;

    @Column
    private BigDecimal sum;

    @Column
    private Date dt;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Phone getPhoneId() {
        return PhoneId;
    }

    public void setPhoneId(Phone phoneId) {
        PhoneId = phoneId;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }
}
