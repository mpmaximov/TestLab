package org.max.domain;

import javax.persistence.*;

/**
 * Created by mmaximov on 11/2/15.
 */

@NamedQueries({
    @NamedQuery(name = "byName", query = "from Producers where name=:name")

})

@Entity(name = "Producers")
public class Producer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Producer(String name) { this.name = name; }

    public Producer() { }


}
