package org.max.service;

import org.max.domain.Producer;

/**
 * Created by mmaximov on 11/3/15.
 */
public interface ProducerService {

    public void add(Producer p);
    public Producer find(String name);


}
