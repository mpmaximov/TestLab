package org.max.service;

import org.max.dao.PhoneDao;
import org.max.dao.PhoneJpaDao;
import org.max.domain.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mmaximov on 10/31/15.
 */
@Service
@Qualifier(value = "PhoneServiceImpl")
public class PhoneServiceImpl implements PhoneService {

    @Autowired
    private PhoneDao phoneDao;

    public PhoneServiceImpl() {
    }

    public void add(Phone p) {

        phoneDao.add(p);

    }

    public void sale(Phone p) {
        phoneDao.sale(p);
        System.out.println("Sale " + p.getProducer() + " " + p.getName());
    }

    public List getPhones() {

        return phoneDao.getPhones();
    }
}
