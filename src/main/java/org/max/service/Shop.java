package org.max.service;

import org.max.domain.Phone;
import org.max.domain.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

/**
 * Created by mmaximov on 11/1/15.
 */
@Service
@Qualifier(value = "shop")
public class Shop implements Runnable {


    @Autowired
    private PhoneService ps;
    @Autowired
    private ProducerService prs;
    private List<Phone> lp;

    public Shop() {

    }

    public void fill() {

        prs.add(new Producer("Lg"));
        prs.add(new Producer("Samsung"));
        prs.add(new Producer("Apple"));

        //TODO do fill by random numbers over the field of enum producers
        Phone p1 = new PhoneBuilder().name("L9").producer("Lg").price(new BigDecimal(10000)).count(10).margin(new BigDecimal(30)).build();
        ps.add(p1);
        Phone p2 = new PhoneBuilder().name("S6").producer("Samsung").price(new BigDecimal(15000)).count(10).margin(new BigDecimal(20)).build();
        ps.add(p2);
        Phone p3 = new PhoneBuilder().name("iPhone6s").producer("Apple").price(new BigDecimal(20000)).count(10).margin(new BigDecimal(10)).build();
        ps.add(p3);

        getPhones();

    }

    private void getPhones() {
        lp = ps.getPhones();
    }

    public void sale() throws Exception {
        int v = new Random().nextInt(lp.size());
        int cnt = 0;
        while (true) {
            Phone p= lp.get(v);
            if (p.getCount() > 0) {
                ps.sale(p);
                break;
            } else {
                v = (v==lp.size())? 0 : v+1;
            }
            cnt+=1;
            if (cnt > lp.size()) new Exception("Shop is empty");
        }
    }

    public void run() {
        while (true) {
            try {
                sale();
                Thread.sleep(1000);
            } catch (Exception e) {
                //e.printStackTrace();
                System.exit(0);
            }
        }
    }
}
