package org.max.service;

import org.max.dao.ProducerDao;
import org.max.dao.ProducerJpaDao;
import org.max.domain.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by mmaximov on 11/3/15.
 */

@Service
@Qualifier(value = "ProducerServiceImpl")
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    private ProducerDao dao;

    public ProducerServiceImpl() {
    }

    public void add(Producer p) {
        dao.add(p);
    }

    public Producer find(String name) {
        return dao.find(name);
    }
}
