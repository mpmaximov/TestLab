package org.max.service;

import com.oracle.webservices.internal.api.databinding.Databinding;
import org.max.dao.ProducerDao;
import org.max.dao.ProducerJpaDao;
import org.max.domain.Phone;
import org.max.domain.Producer;

import java.math.BigDecimal;

/**
 * Created by mmaximov on 11/1/15.
 */
public class PhoneBuilder {

    private long id;
    private String name;
    private Producer producer;
    private BigDecimal price;
    private int count;
    private BigDecimal margin;

    public PhoneBuilder id (long id) { this.id = id;  return this; }
    public PhoneBuilder name (String name) { this.name = name;  return this; }
    public PhoneBuilder producer (Producer producer) { this.producer = producer;  return this; }
    public PhoneBuilder producer (String producer) {
        ProducerDao pd = new ProducerJpaDao();
        this.producer = pd.find(producer);
        return this;
    }
    public PhoneBuilder price (BigDecimal price) { this.price = price;  return this; }
    public PhoneBuilder count (int count) { this.count = count;  return this; }
    public PhoneBuilder margin (BigDecimal margin) { this.margin = margin;  return this; }
    public Phone build() { return Builder(this); }

    private Phone Builder(PhoneBuilder b) {
        Phone p = new Phone();
        p.setId(b.id);
        p.setName(b.name);
        p.setProducer(b.producer);
        p.setPrice(b.price);
        p.setCount(b.count);
        p.setMargin(b.margin);

        return p;
    }
}


