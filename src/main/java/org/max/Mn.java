package org.max;
import org.max.domain.Phone;
import org.max.service.*;
import org.max.util.HibernateUtil;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;
import java.util.List;


/**
 * Created by mmaximov on 10/7/15.
 */
public class Mn {

    public static void main(String[] args) {

        /*if (args.length > 0) {
            String m = args[0].toLowerCase();
            if (m.equals("a") || m.equals("b")) {
                System.out.println((args[0].toLowerCase().equals("a"))?"Привет из профайла A":"Привет из профайла B");
            }
            else System.out.println("Incorrect argument");

        }
        else {
            System.out.println("No arguments given ! ! !");
        }*/

        //ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(org.max.config.Config.class);
        Shop s = (Shop) context.getBean("shop");
        s.fill();
        s.run();

    }

}
