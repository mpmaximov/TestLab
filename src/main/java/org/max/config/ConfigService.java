package org.max.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

/**
 * Created by mmaximov on 11/21/15.
 */
@Configuration
@ComponentScan("org.max.service")
public class ConfigService {
}
