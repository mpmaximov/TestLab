package org.max.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by mmaximov on 11/21/15.
 */
@Configuration
@Import(value = {ConfigDao.class, ConfigService.class})
public class Config {
}
