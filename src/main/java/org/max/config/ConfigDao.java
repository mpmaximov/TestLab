package org.max.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by mmaximov on 11/21/15.
 */
@Configuration
@ComponentScan(value = "org.max.dao")
public class ConfigDao {
}
