package org.max.dao;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.max.domain.Producer;
import org.max.util.HibernateUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.management.Query;

/**
 * Created by mmaximov on 11/2/15.
 */
@Repository
@Qualifier(value = "ProducerJpaDao")
public class ProducerJpaDao implements ProducerDao {


    public void add(Producer p) {
        Session s = HibernateUtil.connect().openSession();
        s.beginTransaction();
        s.save(p);
        s.getTransaction().commit();
        s.close();
    }

    public Producer find(String name) {

        Session s = HibernateUtil.connect().openSession();
        org.hibernate.Query q = s.getNamedQuery("byName");
        q.setString("name", name);
        q.setMaxResults(1);
        Producer p = (Producer) q.list().get(0);
        s.close();

        return p;
    }
}
