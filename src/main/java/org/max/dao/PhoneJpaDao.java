package org.max.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.max.domain.Phone;
import org.max.domain.Sales;
import org.max.util.HibernateUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.management.Query;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by mmaximov on 10/31/15.
 */
@Repository
@Qualifier(value = "PhoneJpaDao")
public class PhoneJpaDao implements PhoneDao {

    public void add(Phone p) {
        Session sf = HibernateUtil.connect().openSession();
        sf.beginTransaction();
        sf.save(p);
        sf.getTransaction().commit();
        //HibernateUtil.disconnect();
        sf.close();
    }

    public void sale(Phone p) {
        Session sf = HibernateUtil.connect().openSession();

        sf.beginTransaction();
        p.setCount(p.getCount()-1);
        sf.update(p);

        Sales s = new Sales();
        s.setPhoneId(p);
        s.setSum(p.getPrice());
        s.setDt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        sf.save(s);
        sf.getTransaction().commit();
        sf.close();
    }

    public List getPhones() {

        Session sf = HibernateUtil.connect().openSession();
        org.hibernate.Query q = sf.getNamedQuery("getAllPhones");
        List l =  q.list();
        sf.close();
        return l;
    }


}
