package org.max.dao;

import org.max.domain.Phone;

import java.util.List;

/**
 * Created by mmaximov on 10/31/15.
 */
public interface PhoneDao {

    public void add(Phone p);
    public void sale(Phone p);
    public List getPhones();

}
