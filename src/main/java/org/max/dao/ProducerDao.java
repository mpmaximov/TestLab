package org.max.dao;

import org.max.domain.Producer;

/**
 * Created by mmaximov on 11/2/15.
 */
public interface ProducerDao {

    public void add(Producer p);
    public Producer find(String name);

}
